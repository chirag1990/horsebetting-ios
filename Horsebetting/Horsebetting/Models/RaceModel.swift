//
//  RaceModel.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import Foundation



// MARK: - RaceModel
struct RaceModel: Codable {
    let raceSummary: RaceSummary
    let rides: [Rides]

    enum CodingKeys: String, CodingKey {
        case raceSummary = "race_summary"
        case rides
    }
}

// MARK: - RaceSummary
struct RaceSummary: Codable {
    let name, courseName, age, distance: String
    let date, time: String
    let rideCount: Int
    let raceStage, going: String
    let hasHandicap, hidden: Bool

    enum CodingKeys: String, CodingKey {
        case name
        case courseName = "course_name"
        case age, distance, date, time
        case rideCount = "ride_count"
        case raceStage = "race_stage"
        case going
        case hasHandicap = "has_handicap"
        case hidden
    }
}

// MARK: - Ride
struct Rides: Codable {
    let clothNumber: Int
    let horse: Horse
    let formsummary, handicap, currentOdds: String
    let withdrawn: Bool?

    enum CodingKeys: String, CodingKey {
        case clothNumber = "cloth_number"
        case horse, formsummary, handicap
        case currentOdds = "current_odds"
        case withdrawn
    }
}

// MARK: - Horse
struct Horse: Codable {
    let name: String
    let daysSinceLastRun: Int?
    let foaled, sex: String
    let lastRanDays: Int?

    enum CodingKeys: String, CodingKey {
        case name
        case daysSinceLastRun = "days_since_last_run"
        case foaled, sex
        case lastRanDays = "last_ran_days"
    }
}
