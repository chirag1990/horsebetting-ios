//
//  UserModel.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import Foundation

// MARK: - LoginModel
struct UserModel: Codable {
    let users: [User]
}

// MARK: - User
struct User: Codable {
    let email, password, name: String
}
