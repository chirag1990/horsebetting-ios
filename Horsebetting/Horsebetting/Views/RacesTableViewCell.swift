//
//  RacesTableViewCell.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import UIKit

class RacesTableViewCell: UITableViewCell {

    @IBOutlet weak var raceNameLabel: UILabel!
    @IBOutlet weak var raceCourseNameLabel: UILabel!
    @IBOutlet weak var raceDateTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.TableViewCells.RacesTableViewCell.XibName, bundle: nil)
    }
    
    func configureCell(raceName: String, raceCourseName: String, raceDate: String) {
        
        raceNameLabel.text = "Name: \(raceName)"
        raceCourseNameLabel.text = "Course Name: \(raceCourseName)"
        raceDateTimeLabel.text = raceDate
        
    }
}
