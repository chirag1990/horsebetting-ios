//
//  RidesCollectionViewCell.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import UIKit

class RidesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var horseImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var clothLabel: UILabel!
    @IBOutlet weak var oddsLabel: UILabel!
    @IBOutlet weak var formLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.CollectionViewCells.RidesCollectionViewCell.XibName, bundle: nil)
    }
    
    func configureUI() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = 5
    }
    
    func configureCell(name: String, cloth: Int, odds: String, form: String, image: UIImage) {
        nameLabel.text = "Name - \(name)"
        clothLabel.text = "Cloth - \(cloth)"
        oddsLabel.text = "Odds - \(odds)"
        formLabel.text = "Form - \(form)"
        horseImageView.image = image
    }
}
