//
//  RaceServices.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import Foundation
import Alamofire

typealias RacesCompletionHandler = (_ races: RaceModel?, _ status: Bool, _ message: String) -> Void

class RaceServices {
    
    public static let shared = RaceServices()
    
    func fetchRaces(completionHandler: @escaping RacesCompletionHandler) {
        
        let url = Constants.APIURL.Races_URL
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            guard let data = responseData.data else {
                return
            }
            do {
                let races = try JSONDecoder().decode(RaceModel.self, from: data)
                completionHandler(races, true, "")
                
            } catch {
                completionHandler(nil, false, error.localizedDescription)
            }
        }
    }
}
