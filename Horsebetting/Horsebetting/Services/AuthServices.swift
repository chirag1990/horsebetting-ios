//
//  AuthServices.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import Foundation

typealias UserInformationCompletionHandler = (_ users: UserModel?, _ status: Bool, _ message: String) -> Void
typealias UserCompletionHandler = (_ users: User?, _ status: Bool, _ message: String) -> Void

class AuthServices {
    
    public static let shared = AuthServices()
    
    var users: UserModel?
    var loggedInUserDetails: User?
    
    func getUserInformation(completionHandler: @escaping UserInformationCompletionHandler) {
        
        guard let path = Bundle.main.path(forResource: "Users", ofType: "json") else {
            print("Users JSON File not found")
            return
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            users = try JSONDecoder().decode(UserModel.self, from: data)
            completionHandler(users, true, "")
        } catch {
            print("in catch \(error)")
            completionHandler(nil, false, error.localizedDescription)
        }
    }
    
    func logIn(with email: String, password: String, completionHandler: @escaping UserCompletionHandler) {
        self.getUserInformation { (userModel, status, message) in
            if status {
                
                guard let userModel = userModel else {
                    print("error doing guard let for userModel")
                    return
                }
                
                for user in userModel.users {
                    if user.email == email && user.password == password {
                        print("We found a user that matches.")
                        self.loggedInUserDetails = user
                        break
                    }
                }
                
                if self.loggedInUserDetails == nil {
                    print("We could not find a user that matches email = \(email)")
                    completionHandler(nil, false, "Could not find the user")
                } else {
                    completionHandler(self.loggedInUserDetails, true, "User Found")
                }
            }
        }
    }
}
