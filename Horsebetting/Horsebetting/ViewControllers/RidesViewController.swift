//
//  RidesViewController.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import UIKit

class RidesViewController: UIViewController {
    
    var races: [RaceModel]?
    var rides: [Rides]?
    var selectedRide: Rides?
    
    @IBOutlet weak var sortClothesButton: SortButton!
    @IBOutlet weak var sortFormButton: SortButton!
    @IBOutlet weak var sortOddsButton: SortButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let neutralSortImage = UIImage(named: "sort_neutral_green") ?? UIImage()
    let ascendSortImage = UIImage(named: "sort_up_green") ?? UIImage()
    let descendSortImage = UIImage(named: "sort_down_green") ?? UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.Strings.RidesViewController.Title
        configureCollectionViewCell()
        configureSortButtonsUI()
    }
    
    func configureCollectionViewCell() {
        collectionView.register(RidesCollectionViewCell.nib(), forCellWithReuseIdentifier: Constants.CollectionViewCells.RidesCollectionViewCell.CellIdentifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func configureSortButtonsUI() {
        
        setSortButtonWithSortIcon(for: sortClothesButton, imageToSet: neutralSortImage)
        setSortButtonWithSortIcon(for: sortFormButton, imageToSet: neutralSortImage)
        setSortButtonWithSortIcon(for: sortOddsButton, imageToSet: neutralSortImage)
    }
    
    func setSortButtonWithSortIcon(for button: SortButton, imageToSet: UIImage) {
        button.setImage(imageToSet, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 10,left: 118,bottom: 10,right: 0)
    }
    
    private func sortButtonPressed(button: SortButton, ascendSort: [Rides], descendSort: [Rides]) {
        
        var imageToSet = UIImage()
        switch button.sortType {
        case .neutral, .descend:
            button.sortType = .ascend
            imageToSet = ascendSortImage
            self.rides = ascendSort
            break
        case .ascend:
            button.sortType = .descend
            imageToSet = descendSortImage
            self.rides = descendSort
            break
        }
        
        setSortButtonWithSortIcon(for: button, imageToSet: imageToSet)

        self.collectionView.reloadData()
    }
    
    
    @IBAction func sortClothesPressed(_ sender: SortButton) {
        let ascendSort = self.rides?.sorted(by: { $0.clothNumber < $1.clothNumber }) ?? []
        let descendSort = self.rides?.sorted(by: { $0.clothNumber > $1.clothNumber }) ?? []
        sortButtonPressed(button: sender, ascendSort: ascendSort, descendSort: descendSort)
    }
    
    @IBAction func sortFormPressed(_ sender: SortButton) {
        let ascendSort = self.rides?.sorted(by: { $0.formsummary < $1.formsummary }) ?? []
        let descendSort = self.rides?.sorted(by: { $0.formsummary > $1.formsummary }) ?? []
        sortButtonPressed(button: sender, ascendSort: ascendSort, descendSort: descendSort)
    }
    
    @IBAction func sortOddsPressed(_ sender: SortButton) {
        let ascendSort = self.rides?.sorted(by: { $0.currentOdds < $1.currentOdds }) ?? []
        let descendSort = self.rides?.sorted(by: { $0.currentOdds > $1.currentOdds }) ?? []
        sortButtonPressed(button: sender, ascendSort: ascendSort, descendSort: descendSort)
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.RidesDetailsVCSegue {
            if let ridesDetailsVC = segue.destination as? RidesDetailsViewController {
                ridesDetailsVC.rides = self.selectedRide
            }
        }
     }
}

extension RidesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedRide = self.rides?[indexPath.row]
        performSegue(withIdentifier: Constants.Segues.RidesDetailsVCSegue, sender: nil)
    }
}

extension RidesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.rides?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCells.RidesCollectionViewCell.CellIdentifier, for: indexPath) as? RidesCollectionViewCell {
            
            guard let rides = self.rides else {
                print("Unable to unwrap rides")
                return UICollectionViewCell()
            }
            
            let horseName = rides[indexPath.row].horse.name
            let cloth = rides[indexPath.row].clothNumber
            let odds = rides[indexPath.row].currentOdds
            let form = rides[indexPath.row].formsummary
            let imageToLoad = UIImage(named: "\(indexPath.row)") ?? UIImage()
            
            cell.configureUI()
            cell.configureCell(name: horseName, cloth: cloth, odds: odds, form: form, image: imageToLoad)
            
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension RidesViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.frame.size.width - 20)/2.2, height: (self.collectionView.frame.size.height)/3)
    }
}
