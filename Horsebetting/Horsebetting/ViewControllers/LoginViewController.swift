//
//  ViewController.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import UIKit
import SkyFloatingLabelTextField
import KeychainSwift
import LocalAuthentication

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var emailTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var loginButton: UIButton!
    
    var user: User?
    
    let keychain = KeychainSwift(keyPrefix: Keys.accessGroup)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureLoginButtonUI()
        configureTextFieldDelegate()
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaultsManager.shared.isBiometricAuthEnabled {
            authenticateUserWithBiometrics()
        }
    }
    
    private func configureLoginButtonUI() {
        loginButton.layer.cornerRadius = 20
    }
    
    private func configureTextFieldDelegate() {
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        emailTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    // MARK: - Login
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        if let email = emailTextfield.text, let password = passwordTextfield.text {
            if password.isEmpty && !email.isEmailValid() {
                self.showAlert(withTitle: Constants.Alerts.ErrorTitle, message: Constants.Alerts.LoginAuthMandatory)
            } else {
                // if not empty performSegue
                logInUser(with: email, password: password) { (user, success, message) in
                    if success {
                        guard let user = user else {
                            print("unable to unwrap user")
                            return
                        }
                        self.user = user
                        
                        if self.keychain.get(Keys.password) != password {
                            self.keychain.delete(Keys.email)
                            self.keychain.delete(Keys.password)
                            self.keychain.set(email, forKey: Keys.email)
                            self.keychain.set(password, forKey: Keys.password)
                        }
                        self.presentHomeViewController()
                    } else {
                        self.showAlert(withTitle: Constants.Strings.LoginViewController.LoginFailed, message: message)
                    }
                }
            }
        }
    }
    
    
    // MARK: - Biometrics
    func authenticateUserWithBiometrics() {
        BiometricsHelper.shared.loginWithBiometrics { (success, message) in
            if !success {
                self.showAlert(withTitle: "Error", message: message ?? "Unknown Error")
            } else {
                DispatchQueue.main.async {
                    self.presentHomeViewController()
                }
            }
        }
    }
}

// MARK: - UITextfieldDelegate

extension LoginViewController: UITextFieldDelegate {
    // Sorting out the keyboard next and done button
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTextfield:
            self.passwordTextfield.becomeFirstResponder()
        default:
            self.passwordTextfield.resignFirstResponder()
        }
        
        return true
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextField {
                if !text.isEmailValid() {
                    floatingLabelTextField.errorMessage = Constants.Strings.LoginViewController.InvalidEmail
                }
                else {
                    // The error message will only disappear when we reset it to nil or empty string
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
    }
}

