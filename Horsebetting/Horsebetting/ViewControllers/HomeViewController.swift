//
//  HomeViewController.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import UIKit
import LocalAuthentication
import SCLAlertView


class HomeViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var biometricText : String = ""
    var races: [RaceModel]?
    var rides: [Rides]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Constants.Strings.HomeViewController.Title
        
        configureBiometrics()
        configureTableView()
        fetchRaces()
    }
    
    func configureBiometrics() {
        biometricText = BiometricsHelper.shared.setUpTextForBiometricSettings()
        showBiometricAlert()
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(RacesTableViewCell.nib(), forCellReuseIdentifier: Constants.TableViewCells.RacesTableViewCell.CellIdentifier)
    }
    
    func fetchRaces() {
        RaceServices.shared.fetchRaces { (raceModel, success, message) in
            if success {
                guard let races = raceModel else { return }
                self.races = Array(arrayLiteral: races)
                self.tableView.reloadData()
            }
        }
    }
    
    func showBiometricAlert() {
        DispatchQueue.main.async {
            if !UserDefaultsManager.shared.hasBiometricAlertShown {
                
                let appearance = SCLAlertView.SCLAppearance(
                    kCircleIconHeight: 50, showCloseButton: false,
                    showCircularIcon: true
                )
                
                let alertView = SCLAlertView(appearance: appearance)
                
                var alertViewIcon : UIImage = UIImage()
                var subTitle: String = ""
                
                if self.biometricText == Constants.Strings.HomeViewController.BiometricFaceID {
                    alertViewIcon = UIImage(named: Constants.Strings.HomeViewController.BiometricFaceIDImage) ?? UIImage()
                    subTitle = Constants.Strings.HomeViewController.BiometricFaceIDMessage
                    
                } else if self.biometricText == Constants.Strings.HomeViewController.BiometricTouchID {
                    alertViewIcon = UIImage(named: Constants.Strings.HomeViewController.BiometricTouchIDImage) ?? UIImage()
                    subTitle = Constants.Strings.HomeViewController.BiometricTouchIDMessage
                }
                
                alertView.addButton(Constants.Strings.HomeViewController.BiometricEnable, backgroundColor: UIColor.systemRed, textColor: UIColor.white) {
                    UserDefaultsManager.shared.isBiometricAuthEnabled = true
                }
                
                alertView.addButton(Constants.Strings.HomeViewController.BiometricDisable, backgroundColor: UIColor.white, textColor: UIColor.systemRed) {
                    UserDefaultsManager.shared.isBiometricAuthEnabled = false
                }
                
                UserDefaultsManager.shared.hasBiometricAlertShown = true
                alertView.showCustom(Constants.Strings.HomeViewController.BiometricsTitle, subTitle: subTitle, color: .white, icon: alertViewIcon)
                
            }
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.RidesVCSegue {
            if let ridesVC = segue.destination as? RidesViewController {
                ridesVC.races = self.races
                ridesVC.rides = self.rides
            }
        }
     }
    
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rides = self.races?[indexPath.row].rides
        self.performSegue(withIdentifier: Constants.Segues.RidesVCSegue, sender: nil)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.races?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.RacesTableViewCell.CellIdentifier) as? RacesTableViewCell {
            
            let raceName = self.races?[indexPath.row].raceSummary.name ?? ""
            let raceCourseName = self.races?[indexPath.row].raceSummary.courseName ?? ""
            
            let date = self.races?[indexPath.row].raceSummary.date ?? ""
            let time = self.races?[indexPath.row].raceSummary.time ?? ""
            let appendedDate = self.appendDateTime(date: date, time: time)
            
            let raceDate = appendedDate.toString(dateFormat: Constants.Date.StringDateFormat)
            
            cell.configureCell(raceName: raceName, raceCourseName: raceCourseName, raceDate: raceDate)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
