//
//  RidesDetailsViewController.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 27/05/2021.
//

import UIKit
import Lottie

class RidesDetailsViewController: UIViewController {
    
    @IBOutlet weak var animationView: AnimationView!
    
    var rides: Rides?
    @IBOutlet weak var horseNameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var lastRanLabel: UILabel!
    @IBOutlet weak var foaledLabel: UILabel!
    
    @IBOutlet weak var clothesLabel: UILabel!
    @IBOutlet weak var oddsLabel: UILabel!
    @IBOutlet weak var formLabel: UILabel!
    @IBOutlet weak var handicapLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Constants.Strings.RidesDetailsViewConrtoller.Title
        configureAnimationView()
        configureUI()
    }
    
    func configureAnimationView() {
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.play()
        animationView.backgroundBehavior = .pauseAndRestore
    }
    
    func configureUI() {
        guard let rides = rides else {
            print("Unable to unwrap rides")
            return
        }
        let lastRan = String(rides.horse.daysSinceLastRun ?? 0)
        
        horseNameLabel.text = "Name - \(rides.horse.name)"
        sexLabel.text = "Sex - \(rides.horse.sex)"
        lastRanLabel.text = "Last Ran - \(lastRan)"
        foaledLabel.text = "Foaled - \(rides.horse.foaled)"
        
        clothesLabel.text = "Clothes - \(rides.clothNumber)"
        oddsLabel.text = "Odds - \(rides.currentOdds)"
        formLabel.text = "Form - \(rides.formsummary)"
        handicapLabel.text = "Handicap - \(rides.handicap)"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(oddsLabelTapped))
        oddsLabel.isUserInteractionEnabled = true
        oddsLabel.addGestureRecognizer(tap)
    }
    
    @objc func oddsLabelTapped() {
        guard let url = URL(string: Constants.WebsiteURL.Skybet_URL) else { return }
        UIApplication.shared.open(url)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
