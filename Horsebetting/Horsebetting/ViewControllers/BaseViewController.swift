//
//  BaseViewController.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import UIKit
import LocalAuthentication

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func logInUser(with email: String, password: String, completionHandler: @escaping UserCompletionHandler) {
        AuthServices.shared.logIn(with: email, password: password, completionHandler: completionHandler)
    }
    
    func presentHomeViewController() {
        if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardIdentifier.HomeViewController) as? HomeViewController {
            let navController = UINavigationController(rootViewController: homeVC)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated:true, completion: nil)
        } else {
            self.showAlert(withTitle: Constants.Alerts.ErrorTitle, message: Constants.Alerts.PresentHomeVcError)
        }
    }
    
    func presentLoginViewController() {
        if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardIdentifier.LoginViewController) as? LoginViewController {
            let navController = UINavigationController(rootViewController: loginVC)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated:true, completion: nil)
        } else {
            self.showAlert(withTitle: Constants.Alerts.ErrorTitle, message: Constants.Alerts.PresentLoginVcError)
        }
    }
    
    func appendDateTime(date: String, time: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = Constants.Date.DateFormat
        let string = date + " at " + time
        let finalDate = dateFormatter.date(from: string)
        
        return finalDate ?? Date()
    }
}
