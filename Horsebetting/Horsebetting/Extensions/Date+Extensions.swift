//
//  Date+Extensions.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import Foundation

extension Date {
    
    func toString( dateFormat format  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
