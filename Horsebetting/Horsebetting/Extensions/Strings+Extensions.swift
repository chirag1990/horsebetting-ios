//
//  Strings+Extensions.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import Foundation

extension String {
    func isEmailValid()->Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return  NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
}
