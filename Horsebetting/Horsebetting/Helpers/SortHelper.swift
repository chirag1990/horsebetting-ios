//
//  SortHelper.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 27/05/2021.
//

import UIKit

public enum SortState: String {
    case neutral
    case ascend
    case descend
}

class SortButton: UIButton {
    var sortType : SortState = .neutral
}
