//
//  BiometricsHelper.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 26/05/2021.
//

import Foundation
import LocalAuthentication
import KeychainSwift

typealias loginCompletionHandler = (_ status: Bool, _ error: String?) -> Void

class BiometricsHelper {
    
    public static let shared = BiometricsHelper()
    
    let keychain = KeychainSwift(keyPrefix: Keys.accessGroup)
    
    let context = LAContext()
    let myLocalizedReasonString = Constants.Strings.LoginViewController.BiometricLocalizedReason
    var error: NSError?
    var didLogIn: Bool = false
    
    func loginWithBiometrics(completionHandler: @escaping loginCompletionHandler) {
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            if let email = self.keychain.get(Keys.email),
               let password = self.keychain.get(Keys.password) {
                
                AuthServices.shared.logIn(with: email, password: password) { (users, success, message) in
                    if !success {
                        //TODO sign user out.
                    } else {
                        self.context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: self.myLocalizedReasonString) { (success, evaluateError) in
                            if success {
                                UserDefaultsManager.shared.isBiometricAuthEnabled = true
                                completionHandler(success, "")
                            } else {
                                if let error = evaluateError {
                                    let message = self.showErrorMessageForLAErrorCode(error: error)
                                    completionHandler(success, message)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showErrorMessageForLAErrorCode(error: Error ) -> String {
        var message = ""
        
        switch error {
        case LAError.appCancel:
            message = Constants.Strings.LoginViewController.BiometricAppCancelError
        case LAError.authenticationFailed:
            message = Constants.Strings.LoginViewController.BiometricAuthFailedError
        case LAError.invalidContext:
            message = Constants.Strings.LoginViewController.BiometricInvalidContextError
        case LAError.passcodeNotSet:
            message = Constants.Strings.LoginViewController.BiometricPasscodeNotSetError
        case LAError.systemCancel:
            message = Constants.Strings.LoginViewController.BiometricSystemCancelError
        case LAError.userCancel:
            message = Constants.Strings.LoginViewController.BiometricUserCancelError
        case LAError.userFallback:
            message = Constants.Strings.LoginViewController.BiometricUserFallbackError
        default:
            message = Constants.Strings.LoginViewController.BiometricDefaultError
        }
        return message
    }
    
    func setUpTextForBiometricSettings() -> String {
        
        var biometricText : String = ""
        
        
        if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            if self.context.biometryType == .faceID {
                biometricText = "Face ID"
            } else if self.context.biometryType == .touchID {
                biometricText = "Touch ID"
            } else {
                biometricText = "None"
            }
        }
        return biometricText
    }
}
