//
//  Constants.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import Foundation

public enum Constants {
    
    public enum APIURL {
        public static let Races_URL = "https://raw.githubusercontent.com/skybet/native-tech-test/master/techtest.json"
    }
    
    public enum WebsiteURL {
        public static let Skybet_URL = "https://m.skybet.com/horse-racing"
    }
    
    public enum TableViewCells {
        public enum RacesTableViewCell {
            public static let XibName = "RacesTableViewCell"
            public static let CellIdentifier = "racesCell"
        }
    }
    
    public enum CollectionViewCells {
        public enum RidesCollectionViewCell {
            public static let XibName = "RidesCollectionViewCell"
            public static let CellIdentifier = "ridesCell"
        }
    }
    
    public enum Alerts {
        public static let ErrorTitle = "Error"
        public static let LoginAuthMandatory = "Both email and password are mandatory."
        public static let PresentHomeVcError = "Cannot instantiate Home view"
        public static let PresentLoginVcError = "Cannot instantiate Login view"
    }
    
    public enum Date {
        public static let DateFormat = "yyyy-MM-dd 'at' hh:mm"
        public static let StringDateFormat = "dd-MM-yyyy 'at' hh:mm"
    }
    
    public enum StoryboardIdentifier {
        public static let HomeViewController = "homeVC"
        public static let LoginViewController = "loginVC"
    }
    
    public enum Segues {
        public static let RidesVCSegue = "ridesSegue"
        public static let RidesDetailsVCSegue = "ridesDetailsSegue"
    }
    
    public enum Strings {
        
        public enum LoginViewController {
            public static let LoginFailed = "Login Failed"
            public static let InvalidEmail = "Invalid Email"
            
            public static let BiometricLocalizedReason = "Use biometrics to authenticate yourself."
            public static let BiometricAppCancelError = "Authentication was cancelled by application"
            public static let BiometricAuthFailedError = "The user failed to provide valid credentials"
            public static let BiometricInvalidContextError = "The context is invalid"
            public static let BiometricPasscodeNotSetError = "Passcode is not set on the device"
            public static let BiometricSystemCancelError = "Authentication was cancelled by the system"
            public static let BiometricUserCancelError = "Authentication was cancelled by the user"
            public static let BiometricUserFallbackError = "The user chose to use the fallback"
            public static let BiometricDefaultError = "Did not find error code on LAError object"
            
        }
        
        public enum HomeViewController {
            public static let Title = "Races"
            public static let BiometricsTitle = "Biometrics"
            
            public static let BiometricFaceID = "Face ID"
            public static let BiometricFaceIDImage = "faceid"
            public static let BiometricFaceIDMessage = "Would you like to enable Face ID to Sign in?"
            
            public static let BiometricTouchID = "Touch ID"
            public static let BiometricTouchIDImage = "touchid"
            public static let BiometricTouchIDMessage = "Would you like to enable Touch ID to Sign in?"
            
            public static let BiometricEnable = "Enable"
            public static let BiometricDisable = "Disable"
        }
        
        public enum RidesViewController {
            public static let Title = "Rides"
        }
        
        public enum RidesDetailsViewConrtoller {
            public static let Title = "Rides Details"
        }
    }
}
