//
//  UserDefaultsManager.swift
//  Horsebetting
//
//  Created by Chirag Tailor on 25/05/2021.
//

import Foundation

struct Keys {
    static let accessGroup = "horse-betting_"
    static let email = "email"
    static let password = "password"
}

public class UserDefaultsManager {
    
    public static let shared: UserDefaultsManager = UserDefaultsManager()
    
    let userDefaults: UserDefaults = .standard
    
    fileprivate struct Keys {
        
        private static let keysPrefix: String = "horseBetting.userDefaultsKeys."
        fileprivate static let biometricAuthEnabled: String = keysPrefix + "biometricAuthEnabled"
        fileprivate static let biometricAlertShown: String = keysPrefix + "biometricAlertShown"
    }
    
    
    
    public var isBiometricAuthEnabled: Bool {
        get {
            return userDefaults.bool(forKey: Keys.biometricAuthEnabled)
        }
        set {
            userDefaults.set(newValue, forKey: Keys.biometricAuthEnabled)
        }
    }
    
    public var hasBiometricAlertShown: Bool {
        get {
            return userDefaults.bool(forKey: Keys.biometricAlertShown)
        }
        set {
            userDefaults.set(newValue, forKey: Keys.biometricAlertShown)
        }
    }
}
