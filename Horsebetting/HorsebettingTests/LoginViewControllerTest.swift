//
//  LoginViewControllerTest.swift
//  HorsebettingTests
//
//  Created by Chirag Tailor on 27/05/2021.
//

import XCTest
@testable import Horsebetting

class LoginViewControllerTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPasswordFieldIsEmpty() {
        let password = ""
        
        let result = password.isEmpty
        XCTAssert(result == true, "password field is empty")
    }
    
    
    func testPasswordField_IsNotEmpty() {
        let password = "test123"
        let result = password.isEmpty
        XCTAssert(result == false, "password field is empty.")
    }
    
    
    func testEmailAddressEnteredIsNotValidEmail() {
        let email = "test@test"
        let result = email.isEmailValid()
        XCTAssert(result == false, "email should not be valid")
    }
    
    func testEmailAddressEnteredIsValidEmail() {
        let email = "test@test.com"
        let result = email.isEmailValid()
        XCTAssert(result == true, "email should be valid")
    }
    
    func testUserDidNotEnterEmailAddress() {
        let email = ""
        let result = email.isEmailValid()
        XCTAssert(result == false, "email should not be valid")
    }
    
    func testUserEnteredBothEmailAndPassword() {
        let email = "test@test.com"
        let password = "12345"
        
        let result = email.isEmailValid() && !password.isEmpty
        XCTAssert(result == true, "email and passowrd is Entered")
    }
    
    func testUserEnteredEmail_ButNotPassword() {
        let email = "test@test.com"
        let password = ""
        
        let result = email.isEmailValid() && !password.isEmpty
        XCTAssert(result == false, "email and passowrd is Entered")
    }
    
    func testUserEnteredPassword_ButNotEmail() {
        let email = ""
        let password = "12345"
        
        let result = email.isEmailValid() && !password.isEmpty
        XCTAssert(result == false, "email is not entered")
    }


}
